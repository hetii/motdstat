PROGRAMS=motdstat
CONFIGS=motdstat.conf


usage:
	@echo "This makefile allows you to:"
	@echo "  o install MOTDstat"
	@echo "  o unintall MOTDstat"
	@echo
	@echo Examples:
	@echo "  make install"
	@echo "  make uninstall"


install: 
	for prog in $(PROGRAMS); do \
		install -m 0755 bin/$$prog /usr/bin; \
	done

	if test ! -d /etc/motdstat; then mkdir -p /etc/motdstat; fi

	for config in $(CONFIGS); do \
		if test ! -e /etc/motdstat/$$config; then install -m 644 ./etc/motdstat/$$config /etc/motdstat; fi \
	done
	
	/usr/bin/motdstat -g
uninstall:
	for prog in $(PROGRAMS); do \
		rm -f /usr/bin/$$prog; \
	done
	rm -rf /etc/motdstat/ /etc/motd.full
	mv -f /etc/motd.orig /etc/motd
